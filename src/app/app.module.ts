import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import {HttpModule} from '@angular/http';
import {RouterModule,Routes} from '@angular/router'; 
import {OneComponent} from './pages/one/one';
import {TwoComponent} from './pages/two/two';
import {LoginComponent} from './pages/login/login';

const appRoutes: Routes = [
  { path:'login', component:LoginComponent},
  { path: 'one', component: OneComponent },
  { path: 'two', component: TwoComponent },
  { path: '**', component: LoginComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OneComponent,
    TwoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

 }

import { Component } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {RequestOptions} from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Hipcask';
  private headers: Headers;

  constructor (private http: Http){
    this.headers = new Headers();
    this.headers.append('Content-Type','application/json');
    this.headers.append('Accept', 'application/json');
  }

  private email : string;
  private password: string;

  private login() 
  {
      let url ='http://api.hipcask.in/analytics/brewery/login'
      let options = new RequestOptions({headers:this.headers});
      let data = {
        "username":this.email,
        "password": this.password
      }

      this.http.post(url,data,options).subscribe(res=>{
        console.log("The response is");  
        console.log(res);
        alert("You login successfully");
      },
        err=>{
          console.log(err);
          alert("You Entered sometthing wrong")
        });
      console.log("In login function",this.email);
     // this.http.post

  }

}
